#!/bin/bash
#PBS -l nodes=1:ppn=8
#PBS -l walltime=01:00:00
#PBS -N lab_3
#PBS -q batch
#PBS -j oe

cd $PBS_O_WORKDIR
module load gcc
gcc -fopenmp lab3.c -o lab3

for i in {1..8}; do
  export OMP_NUM_THREADS=$i
  echo "$i threads: " >> output.txt
  ./lab3 >> output.txt
done