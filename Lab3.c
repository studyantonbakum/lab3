#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define N 1000 // розмір матриці

void gauss_elimination(double **a, double *b, double *x) {
    int i, j, k;
    for (k = 0; k < N; k++) {
        #pragma omp parallel for shared(a, b, k) private(i, j)
        for (i = k + 1; i < N; i++) {
            double factor = a[i][k] / a[k][k];
            for (j = k; j < N; j++) {
                a[i][j] -= factor * a[k][j];
            }
            b[i] -= factor * b[k];
        }
    }
    
    // Зворотній хід
    for (i = N - 1; i >= 0; i--) {
        x[i] = b[i];
        for (j = i + 1; j < N; j++) {
            x[i] -= a[i][j] * x[j];
        }
        x[i] /= a[i][i];
    }
}

int main() {
    double **a = (double **)malloc(N * sizeof(double *));
    for (int i = 0; i < N; i++) {
        a[i] = (double *)malloc(N * sizeof(double));
    }
    double *b = (double *)malloc(N * sizeof(double));
    double *x = (double *)malloc(N * sizeof(double));

    // Ініціалізація матриць випадковими значеннями
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            a[i][j] = rand() % 100 + 1;
            if (i == j) {
                a[i][j] += N * 10; // Забезпечення доброї обумовленості
            }
        }
        b[i] = rand() % 100 + 1;
    }

    double start_time = omp_get_wtime();
    gauss_elimination(a, b, x);
    double end_time = omp_get_wtime();

    printf("Time taken: %f seconds\n", end_time - start_time);

    // Звільнення пам'яті
    for (int i = 0; i < N; i++) {
        free(a[i]);
    }
    free(a);
    free(b);
    free(x);

    return 0;
}